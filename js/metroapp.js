jQuery(window).load(function() {
    function changeLanguage (){
        $("*").each(function(index, el){
            if($(el).data("text")){
                $(el).html(__($(el).data("text")))
            }
        })
    }


    if(localStorage.getItem('locale') == "fa-ir"){
        $("body").addClass("rtl-style");
    }
    
    $(".change-language a." + localStorage.getItem('locale')).addClass("selected");
    changeLanguage();
   

    $(window).on('hashchange', function(e){
        $(".preloader").show();
        $("body").toggleClass("rtl-style");
        var locale = (localStorage.getItem('locale') == "fa-ir") ? "en-us" : "fa-ir";
        localStorage.setItem('locale', locale);
        $(".change-language a.selected").removeClass("selected");
        $(".change-language a." + localStorage.getItem('locale')).addClass("selected");
        $.getJSON( "/nls/" + localStorage.getItem('locale') + ".json" , function( result ){
            window.__ = function(key){
                return  result[key]
            }
            changeLanguage();
            $(".preloader").hide();
            $(".status").show();
        });
       
    });

});

   